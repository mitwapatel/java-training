package Assignmnet1;


/*
Design a high-level Schema for restaurants having employees as Owners, Manger, assistant manager of each department, Employees part of department.
Departments:Kitchen, Bar, Dining, Accounting, Reception/Security
Consider Manger has controller over all the employees (Hire/Fire any one from restaurant), 
Assistant manager has controller over his department (Hire/ fire anyone from his department only).
Employees from department should be equipped to do their respective task like chiefs- cooking, Servers – serving/waitering, etc.
*/
class Restaurant {
	
	public static void main(String args[]) {
		
		
		Reception reception= new Reception("Marry Jane", 5, "123456ndfkjs", 120000, "9231243232", "Reception");
		reception.employeeInformation();
		reception.role();
		reception.bonus();
		
		Kitchen kitchen = new Kitchen("Harry potter", 8, "123456ndfkjs", 80000, "2323123232", "kitchen");
		kitchen.employeeInformation();
		kitchen.role();
		kitchen.bonus();	
		
		
		Dining dining = new Dining("Pitter Parker", 10, "1234 warren ave, Femont", 89000, "4987645321", "Dining");
		dining.employeeInformation();
		dining.role();
		dining.bonus();
		dining.manager();
		
		Account account = new Account(" Ron wisley", 7, "Hanrey frive, Santa clara", 98200, "9231232320", "Account");
		account.employeeInformation();
		account.role();
		account.bonus();
		
	}

		
	
}
