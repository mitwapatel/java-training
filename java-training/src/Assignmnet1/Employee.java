package Assignmnet1;

public abstract class Employee {
	
	
	String name;
	int hoursPerDay;
	String address;
	double salary;
	String phonenum;
    String department;
	
	Employee(String name, int hourPerDay, String address, double salary,String phonenum, String department){
		
		this.name= name;
		this.hoursPerDay= hourPerDay;
		this.address=address;
		this.salary= salary;
		this.phonenum= phonenum;
		this.department= department;
	}
	
	public void employeeInformation(){
		
		System.out.println("Eployee Name:" +name);
		System.out.println("Daily wokring hours"+ hoursPerDay);
		System.out.println("Salary:" +salary);
		System.out.println("Address: " +address);
		System.out.println("Contact:" +phonenum);
		System.out.println("Department:"+department);
	
		
	}

	public abstract void role();
	
	public float bonus() {
		float bonus=  (float) ((salary*3)/100);
		System.out.println("Bonus: "+bonus+"\n");
		return bonus;
}
}

